#include "convolution.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "logging.h"

#include <iostream>

BLOB* load_weights(BLOB* b, conv_param_t* p) {

	//open weights file for read
	FILE* fp = fopen(p->weights, "rb");
	if (fp == NULL)
		error("could not open file %s for reading\n", p->weights);

	//for fully connected layers the kernel size is equal to the input size
	int Ky = (p->fc) ? b->h : p->Ky;
	int Kx = (p->fc) ? b->w : p->Kx;

	//allocate 3D blob, and emulate 4D in KxKy later
	BLOB* w = blob_alloc(p->num_out, b->d / p->group, Ky * Kx);

	//fill 4D weight structure
	for (int g = 0; g < p->group; g++)
		for (int o = g * (p->num_out / p->group);
				o < (g + 1) * (p->num_out / p->group); o++)
			for (int i = g * (b->d / p->group); i < (g + 1) * (b->d / p->group);
					i++)
				//note: each printf("%d   -    ",p->pad);output map has only  b->d/p->group input maps. Hence the absolute index of i is subtracted when storing in w!
				if ((int) fread(
						&(blob_data(w, o, i - g * (b->d / p->group), 0)),
						sizeof(float), Ky * Kx, fp) != Ky * Kx)
					error("loading weights from file %s\n", p->weights);

	//close file
	fclose(fp);

	//return weight blob
	return w;
}

float* load_1d(const char* fname, size_t num) {

	//open file for reading
	FILE* fp = fopen(fname, "rb");
	if (fp == NULL)
		error("could not open file %s for reading\n", fname);

	//read in array
	float* arr = (float*) malloc(sizeof(float) * num);
	if (fread(arr, sizeof(float), num, fp) != num)
		error("loading data from file %s\n", fname);

	//close file
	fclose(fp);

	return arr;
}

__global__
void padGPU(float* in, float* out, int pad, int img_height, int img_width,
		int img_dim) {
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	int z = blockIdx.z * blockDim.z + threadIdx.z;
	if (x < img_width && y < img_height && z < img_dim)
		out[z * (img_height + (pad * 2)) * (img_width + (pad * 2))
				+ (y + pad) * (img_width + (pad * 2)) + (x + pad)] = in[z
				* img_height * img_width + (y) * img_width + (x)];

}

__global__
void convolutionGPU1(float* in, float* out, float* w, int Ky, int Kx,
		int in_height, int in_width, int in_dim, int out_height, int out_width,
		int out_dim, int w_height, int w_width, int Sx, int Sy) {
	int o = threadIdx.x + blockIdx.x * blockDim.x;
	int n = threadIdx.y + blockIdx.y * blockDim.y;
	int m = threadIdx.z + blockIdx.z * blockDim.z;
	if (o < out_dim && n < out_width && m < out_height)
		for (int i = 0; i < (in_dim); i++)
			for (int k = 0; k < Ky; k++)
				for (int l = 0; l < Kx; l++)
					out[(o) * out_height * out_width + (m) * out_width + (n)] +=
							in[(i) * in_height * in_width
									+ (m * Sy + k) * in_width + (n * Sx + l)]
									* w[(o) * w_height * w_width + (i) * w_width
											+ (k * Kx + l)];
}

__global__
void convolutionGPU2(float* in, float* out, float* w, int group, int in_height,
		int in_width, int in_dim, int out_height, int out_width, int out_dim,
		int w_height, int w_width, int Sx, int Sy) {
	int g = threadIdx.x + blockIdx.x * blockDim.x;
	int m = threadIdx.y + blockIdx.y * blockDim.y;
	int n = threadIdx.z + blockIdx.z * blockDim.z;
	if (g < group && m < out_height && n < out_width)
		for (int o = g; o < (g + 1); o++)
			for (int i = g; i < (g + 1); i++)
				for (int k = 0; k < 3; k++)
					for (int l = 0; l < 3; l++)
						out[(o) * out_height * out_width + (m) * out_width + (n)] +=
								in[(i) * in_height * in_width
										+ (m * Sy + k) * in_width + (n * Sx + l)]
										* w[(o) * w_height * w_width
												+ (i - g) * w_width
												+ (k * 3 + l)];
}

__global__
void meanscaleGPU(float* out, float* mean, float* var, float*scale,
		float*scale_bias, float bn_eps, int out_dim, int out_height,
		int out_width) {
	int o = threadIdx.x + blockIdx.x * blockDim.x;
	int m = threadIdx.y + blockIdx.y * blockDim.y;
	int n = threadIdx.z + blockIdx.z * blockDim.z;
	if (o < out_dim && m < out_height && n < out_width)
		out[(o) * out_height * out_width + (m) * out_width + (n)] = ((out[(o)
				* out_height * out_width + (m) * out_width + (n)] - mean[o])
				/ sqrtf(var[o] + bn_eps) * scale[o] + scale_bias[o]);
}

__global__
void meanscalereluGPU(float* out, float* mean, float* var, float*scale,
		float*scale_bias, float bn_eps, int out_dim, int out_height,
		int out_width) {
	int o = threadIdx.x + blockIdx.x * blockDim.x;
	int m = threadIdx.y + blockIdx.y * blockDim.y;
	int n = threadIdx.z + blockIdx.z * blockDim.z;
	if (o < out_dim && m < out_height && n < out_width)
		out[(o) * out_height * out_width + (m) * out_width + (n)] = fmax(0.0f,
				((out[(o) * out_height * out_width + (m) * out_width + (n)]
						- mean[o]) / sqrtf(var[o] + bn_eps) * scale[o]
						+ scale_bias[o]));
}

//convolution, NOTE: destructive of BLOB* in. duplicate if further required!
BLOB* convolution(BLOB* input, conv_param_t* p) {
//use local pointer
	BLOB* in = input;
	BLOB* out;
	float *device_pad;
	if (p->pad != 0) {
		out = blob_calloc(in->d, in->h + 2 * p->pad, in->w + 2 * p->pad);

		//Allocate GPU mem
		float* device_in;
		blob2gpu(device_in, in);
		blob2gpu(device_pad, out);

		double threadsPerBlockX = 16;
		double threadsPerBlockY = 2;
		double threadsPerBlockZ = 8;
		double numBlocksX = ceil(in->w / threadsPerBlockX);
		double numBlocksY = ceil(in->h / threadsPerBlockY);
		double numBlocksZ = ceil(in->d / threadsPerBlockZ);
		dim3 grids(numBlocksX, numBlocksY, numBlocksZ);
		dim3 blocks(threadsPerBlockX, threadsPerBlockY, threadsPerBlockZ);

		padGPU<<<grids, blocks>>>(device_in, device_pad, p->pad, in->h, in->w,
				in->d);
		in = out;
//		cudaFree(device_in);
	} else {
		blob2gpu(device_pad, in);
	}

//if fully connected, the kernel size is set to the image size
	int Ky = (p->fc) ? in->h : p->Ky;
	int Kx = (p->fc) ? in->w : p->Kx;

//create blob to hold output
	int height = (int) floor(((float) in->h - (float) Ky) / (float) p->Sy) + 1;
	int width = (int) floor(((float) in->w - (float) Kx) / (float) p->Sx) + 1;

//load bias if required
	if (p->bias == NULL) {
		//zero init
		out = blob_calloc(p->num_out, height, width);
	} else {
		//not required to calloc
		out = blob_alloc(p->num_out, height, width);

		//load bias values from file
		float* bias = load_1d(p->bias, p->num_out);

		//set bias or init with zeroes
		for (int o = 0; o < out->d; o++)
			for (int m = 0; m < out->h; m++)
				for (int n = 0; n < out->w; n++)
					blob_data(out,o,m,n)=bias[o];
					//cleanup bias
//		free(bias);
				}

	BLOB* w = load_weights(in, p);

//perform convolution
	float *device_convoluted, *device_w;
	blob2gpu(device_convoluted, out);
	blob2gpu(device_w, w);
	if (p->group == 1) {
		double threadsPerBlockX = 2;
		double threadsPerBlockY = 16;
		double threadsPerBlockZ = 8;
		if (out->d == 1000) {
			threadsPerBlockX = 8;
			threadsPerBlockY = 4;
			threadsPerBlockZ = 1;
		}
		if (out->d == 1280) {
			threadsPerBlockX = 4;
			threadsPerBlockY = 8;
			threadsPerBlockZ = 4;
		}
		double numBlocksX = ceil((out->d) / threadsPerBlockX);
		double numBlocksY = ceil((out->w) / threadsPerBlockY);
		double numBlocksZ = ceil((out->h) / threadsPerBlockZ);
		dim3 grids(numBlocksX, numBlocksY, numBlocksZ);
		dim3 blocks(threadsPerBlockX, threadsPerBlockY, threadsPerBlockZ);

		convolutionGPU1<<<grids, blocks>>>(device_pad, device_convoluted,
				device_w, Ky, Kx, in->h, in->w, in->d, out->h, out->w, out->d,
				w->h, w->w, p->Sx, p->Sy);
	} else {
		double threadsPerBlockX = 1;
		double threadsPerBlockY = 4;
		double threadsPerBlockZ = 32;
		double numBlocksX = ceil((p->group) / threadsPerBlockX);
		double numBlocksY = ceil((out->h) / threadsPerBlockY);
		double numBlocksZ = ceil((out->w) / threadsPerBlockZ);
		dim3 grids(numBlocksX, numBlocksY, numBlocksZ);
		dim3 blocks(threadsPerBlockX, threadsPerBlockY, threadsPerBlockZ);

		convolutionGPU2<<<grids, blocks>>>(device_pad, device_convoluted,
				device_w, p->group, in->h, in->w, in->d, out->h, out->w, out->d,
				w->h, w->w, p->Sx, p->Sy);
	}

//perform batchnorm if needed
	if (p->bn_mean != NULL) {
		//load batchnorm mean and variance
		float* mean = load_1d(p->bn_mean, out->d);
		float* var = load_1d(p->bn_var, out->d);
		float *device_mean, *device_var;
		cudaMalloc(&device_mean, sizeof(float) * out->d);
		cudaMalloc(&device_var, sizeof(float) * out->d);
		cudaMemcpy(device_mean, mean, sizeof(float) * out->d,
				cudaMemcpyHostToDevice);
		cudaMemcpy(device_var, var, sizeof(float) * out->d,
				cudaMemcpyHostToDevice);

		float* scale = load_1d(p->scale, out->d);
		float* scale_bias = load_1d(p->scale_bias, out->d);

		float *device_scale, *device_bias;
		cudaMalloc(&device_scale, sizeof(float) * out->d);
		cudaMalloc(&device_bias, sizeof(float) * out->d);
		cudaMemcpy(device_scale, scale, sizeof(float) * out->d,
				cudaMemcpyHostToDevice);
		cudaMemcpy(device_bias, scale_bias, sizeof(float) * out->d,
				cudaMemcpyHostToDevice);

		double threadsPerBlockX = 2;
		double threadsPerBlockY = 4;
		double threadsPerBlockZ = 16;
		double numBlocksX = ceil(out->d / threadsPerBlockX);
		double numBlocksY = ceil(out->h / threadsPerBlockY);
		double numBlocksZ = ceil(out->w / threadsPerBlockZ);
		dim3 grids(numBlocksX, numBlocksY, numBlocksZ);
		dim3 blocks(threadsPerBlockX, threadsPerBlockY, threadsPerBlockZ);

		if (p->relu == true) {
			meanscalereluGPU<<<grids, blocks>>>(device_convoluted, device_mean,
					device_var, device_scale, device_bias, p->bn_eps, out->d,
					out->h, out->w);
		} else {
			meanscaleGPU<<<grids, blocks>>>(device_convoluted, device_mean,
					device_var, device_scale, device_bias, p->bn_eps, out->d,
					out->h, out->w);
		}
	}
	gpu2blob(out, device_convoluted);
//return output
	return out;
}
